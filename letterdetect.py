#!/usr/bin/env python
import argparse
import logging
import os
from collections import OrderedDict

import colorcet as cs
import cv2
import matplotlib.pyplot as plt
import numpy as np
import yaml
from cbs_utils.misc import (get_logger, create_logger, query_yes_no)

# set here the global variables. Need to be adjusted in the app
MARGIN = 5  # add a margin around each tile which is removed
TOP_SECTION_FRACTION = 0.13  # the size of the digit at the top right of each tile
DIGIT_MASK_HEIGHT_FRACTION = 0.35  # the size of the digit at the top right of each tile
DIGIT_MASK_WIDTH_FRACTION = .30
N_TILES = 15  # the number of tiles (same in vertical and horizontal direction)
TILE_N_PIX = 64  # number of pixel to resample the tile on
N_PIXEL_MINIMUM = 100  # minimum number of white pixels in the digit to consider it valid
VALUE_DIGIT_THRESHOLD = 1
THRESHOLD_VALUE = 30

LETTER_COUNT = dict(
    A=7,
    B=2,
    C=2,
    D=5,
    E=18,
    F=2,
    G=3,
    H=2,
    I=4,
    J=2,
    K=3,
    L=3,
    M=3,
    N=11,
    O=6,
    P=2,
    Q=1,
    R=5,
    S=5,
    T=5,
    U=3,
    V=2,
    W=2,
    X=1,
    Y=1,
    Z=2,
    blanco=2
)


class Letter(object):
    """
    Common base class for all characters
    """

    def __init__(self, char_name, location=(None, None), tile_image=None):

        self.char_name = char_name

        # derive the integer beloning to this character, starting at 0 (A=0, B=1, ... Z=25, blanco=26)
        if char_name == "blanco":
            self.char_code = 26
        else:
            self.char_code = ord(char_name) - ord("A")

        # the location of this letter
        self.location = location

        # the tile threshold binary data
        self.tile_image = tile_image

        # the number of occurance of this letter
        self.count = 0

    def increase_counter(self):
        """
        add one more the counter of this letter
        :return:  nothing
        """

        self.count += 1


class LetterDatabase(object):
    """
    class holding all the letter with routine to read the text file for training and to store the pattern data
    """

    def __init__(self, file_name, training_data_base, mode="analyses", debug_plot=False):

        self.logger = get_logger(__name__)

        self.file_name = file_name
        self.file_base = os.path.splitext(self.file_name)[0]

        self.training_data_base = training_data_base

        self.mode = mode

        self.x_start = None
        self.digit_mask_height = None
        self.digit_mask_width = None
        self.top_section_x = None

        # the letters dictionary is going to contain all the information of the letters
        self.letters = dict()

        self.char_matrix = np.empty((N_TILES, N_TILES), dtype="U")
        self.char_matrix.fill(np.nan)

        # hold the main threshold image for the analyses
        self.image = None
        self.im_original = None

        # the image data base is going to hold the scnreen shot image
        self.screen_shot_data_base = ImageDatabase()

        # the tile  data base is going to contain the images of all the tile
        self.tile_data_base = ImageDatabase()

        if self.mode == "train":
            self.logger.debug("In training mode.")
            self.read_label_file()
        else:
            self.logger.debug("In analysing mode.")
            self.load_training_letters()

        # add an extra entry for the blanco
        self.letters["blanco"] = Letter(char_name="blanco")

        self.square_size = None

        self.read_screen_shot()

        self.get_x_start()

        self.analyse_tiles(debug_plot=debug_plot)

        if self.mode == "train":
            self.logger.debug("Dumping analysed tiles to file")
            self.dump_training_letters()
        else:
            self.logger.debug("Counting the tiles matrix")
            self.count_letters()
            self.make_report()

    def get_x_start(self):
        """
        get the start of the board from the top
        """

        repeated_black = 0
        prev_grey = None
        started_scan = False
        self.logger.debug("dim {}".format(self.image.shape))
        for i_line in range(self.image.shape[0]):
            mean_gray = self.image[i_line, :].mean()

            self.logger.debug(f"line {i_line} {mean_gray} {repeated_black} ")
            if mean_gray < 1:
                repeated_black += 1

            if repeated_black > 100 or started_scan:
                started_scan = True
                self.logger.debug("checking {} {}".format(i_line, mean_gray))
                # we are in the gray area above the tile. Stop as soon as we find a white line
                if mean_gray > 240:
                    self.logger.debug("found {} {}".format(i_line, mean_gray))
                    self.x_start = i_line
                    break
            else:
                self.logger.debug("skipping {} {}".format(i_line, mean_gray))

        if self.x_start is None:
            raise ValueError("Could not find start of board")

    def make_report(self):
        """
        create a report of the counted letters
        :return:
        """
        in_game = OrderedDict()
        self.logger.info("{:5s} : {:10s} {:10s} {:10s}".format("letter", "count", "total", "left"))
        for char in sorted(self.letters.keys()):
            nr = self.letters[char].count
            tot = LETTER_COUNT[char]
            left = tot - nr
            if left > 0:
                in_game[char] = left
            self.logger.info("{:5s} : {:10d} {:10d} {:10d}".format(char, nr, tot, left))

        self.logger.info("-----------------------")
        tot_in_game = np.array(list(in_game.values())).sum()
        self.logger.info("summary of letter left ({})".format(tot_in_game))
        self.logger.info("-----------------------")
        for key, val in in_game.items():
            self.logger.info("{:5s} : {:10d}".format(key, val))

    def count_letters(self):
        """
        We have filled the char matrix. Count the letters
        :return:
        """

        cnt = 0
        for i_row in range(N_TILES):
            for j_col in range(N_TILES):
                char = self.char_matrix[i_row, j_col]
                if char != "n":

                    # here also set the title to the character of the tile

                    self.tile_data_base.image_list[tile_key(i_row, j_col)].title = char
                    cnt += 1

                    if char == "*":
                        # for the * we want to rename it to '
                        char = "blanco"

                    self.letters[char].increase_counter()
                    self.logger.debug("Counted  {} {}".format(char, self.letters[char].count))
                else:
                    self.logger.debug("Skipping empty tile")

    def read_label_file(self):
        """
        this routine read the text file containing the asci respresentation of all characters of a training board
        :return: nothing, just store the letter in the letters dictionary
        """

        # replace the extension of the screen shot with the ".txt' extension
        text_label_file = self.file_base + ".txt"
        self.logger.info("Reading text label file {}".format(text_label_file))
        with open(text_label_file, "r") as fp:
            # loop over the lines (rows) of the text file
            for i_row, line in enumerate(fp.readlines()):
                line = line.strip()
                # loop over the columns (characters) of the current row
                for j_col, char in enumerate(list(line)):
                    # we found a letter. Add it to the list if it has not been added yet. We want only one letter
                    # also skip all dots, as that are the empty fields
                    if char not in list(self.letters.keys()) and char != ".":
                        self.logger.debug(
                            "Adding character {} at row={:2d} col={:2d}".format(char, i_row, j_col))
                        self.letters[char] = Letter(char_name=char, location=(i_row, j_col))

                    # for all the chars: store then in the char matrix
                    self.char_matrix[i_row][j_col] = char

    def read_screen_shot(self):
        """
        read the screen shot and pre process the image to get the thresholded image
        :return: nothing
        """

        logging.info("Reading image file {}".format(self.file_name))
        self.im_original = cv2.imread(self.file_name, 1)

        logging.debug("Image size: {}".format(self.im_original.shape))
        im_gray = cv2.cvtColor(self.im_original, cv2.COLOR_BGR2GRAY)

        self.square_size = im_gray.shape[1] // N_TILES

        self.top_section_x = int(TOP_SECTION_FRACTION * im_gray.shape[0])
        self.logger.debug("found top section {}".format(self.top_section_x))
        self.digit_mask_height = int(self.square_size * DIGIT_MASK_HEIGHT_FRACTION)
        self.digit_mask_width = int(self.square_size * DIGIT_MASK_WIDTH_FRACTION)

        # add the original and gray valued screen shot to the image data base
        self.screen_shot_data_base.add_image_to_dict(self.im_original, key="original",
                                                     title="original", colormap=None)
        self.screen_shot_data_base.add_image_to_dict(im_gray, key="gray", title="gray")

        # threshold the screenshot
        rem, im_threshold = cv2.threshold(im_gray, 40, 255, cv2.THRESH_BINARY_INV)
        #rem, im_threshold = cv2.threshold(im_gray, THRESHOLD_VALUE, 255, cv2.THRESH_BINARY)
        #im_threshold = (255 - im_threshold)
        self.screen_shot_data_base.add_image_to_dict(im_threshold, key="binary", title="binary")

        # store the threshold image in the image fiel
        self.image = im_threshold

    def match_pattern(self, roi, fig=None, axis=None, update_plot=False):
        """
        routine to find the most closely matching character
        :param roi: the current character pattern
        :return:
        """

        char_min_dist = None
        for char in list(self.letters.keys()):
            if char == "blanco":
                continue
            self.logger.debug("Comparing difference with {}".format(char))
            char_pattern = self.letters[char].tile_image
            diff_pattern = (roi.astype(float) - char_pattern.astype(float)) ** 2
            char_dist = np.sum(diff_pattern)
            if update_plot:
                axis[0].imshow(roi)
                axis[1].imshow(char_pattern)
                axis[2].imshow(diff_pattern)
                plt.draw()
                plt.ioff()
                plt.show()
                text = "dist: {}".format(char_dist)
                plt.show()
                query_yes_no(text + "contiunue?", default_answer="yes")
            if char_min_dist is None or char_dist < char_min_dist:
                char_min_dist = char_dist
                char_match = char

        return char_match

    def analyse_tiles(self, debug_plot=False):
        """
        analyse the binary screen shot for the tiles
        :return:
        """

        if debug_plot:
            fig2, axis2 = plt.subplots(nrows=1, ncols=3)
            fig2.canvas.manager.set_window_title("Debug chars")
            plt.show()
        else:
            fig2 = axis2 = None

        # loop over the tiles
        for i_row in range(N_TILES):
            for j_col in range(N_TILES):

                # get the pixel values beloning to the curren tile
                ipx = self.x_start + i_row * self.square_size
                jpy = j_col * self.square_size

                # create the selection of the tile i_row, j_col
                roi = self.image[
                      ipx + MARGIN:ipx + self.square_size - MARGIN,
                      jpy + MARGIN:jpy + self.square_size - MARGIN
                      ]

                # add the current tile to the tile data base
                self.tile_data_base.add_image_to_dict(roi, tile_key(i_row, j_col))

                if roi.sum() // 255 < N_PIXEL_MINIMUM:
                    # there is no letter here. Continue to the next tile
                    continue

                # get the area where we have the counter and see if there any white pixels
                value_digit = roi[:self.digit_mask_height, -self.digit_mask_width:]

                # set flag if this tile with letter is a blanco
                if value_digit.sum() // 255 < VALUE_DIGIT_THRESHOLD:
                    is_blanco = True
                else:
                    is_blanco = False

                # remove the value digit
                value_digit[:, :] = 0

                # crop to the bounding box and sample to a fixed number of pixels
                x_min, x_max, y_min, y_max = get_bounding_box(roi)
                roi_s = roi[y_min: y_max, x_min: x_max]
                roi2 = cv2.resize(roi_s, (TILE_N_PIX, TILE_N_PIX), interpolation=cv2.INTER_CUBIC)

                if self.mode == "train":
                    # we are in training mode. Store each character which has not yet been set
                    char = self.char_matrix[i_row, j_col]

                    self.logger.debug("In training mode with the character found {} ".format(char))

                    if char != "." and self.letters[char].tile_image is None:
                        # we have not yet store the binary data belong to this letter
                        self.logger.debug("Storing this character in the tile image")
                        self.letters[char].tile_image = roi2
                        self.logger.debug(
                            "Shape of data stored {}".format(self.letters[char].tile_image.shape))
                else:
                    self.logger.debug(
                        "In letter analysing mode. See if we can recognise this character")

                    if i_row == 2 and j_col == 0 and debug_plot:
                        update_plot = True
                    else:
                        update_plot = False

                    char = self.match_pattern(roi2, fig=fig2, axis=axis2, update_plot=update_plot)
                    self.logger.debug("Found character {}".format(char))

                    if is_blanco:
                        self.char_matrix[i_row, j_col] = "*"
                    else:
                        self.char_matrix[i_row, j_col] = char

    def pack_letters(self):
        """
        pack the binary tile image to a dictionary
        :return: binary stream
        """
        bin_out = dict()
        for char in sorted(self.letters.keys()):
            self.logger.debug("Writing character {}".format(char))
            bin_out[char] = self.letters[char].tile_image

        return bin_out

    def unpack_letters(self, bin_dict):
        """
        unpack the dictionary with  the digital images to the image object
        :return: binary stream
        """
        for char in sorted(bin_dict.keys()):
            self.logger.debug("Storing binary data to {}".format(char))
            self.letters[char] = Letter(char_name=char, tile_image=bin_dict[char])

    def dump_training_letters(self):
        """
        dump the binary data of the letter data base to file
        :return: nothing
        """
        self.logger.info("Write training data to file {}".format(self.training_data_base))

        bin_dict = self.pack_letters()
        self.logger.debug("Pack dict keys {}".format(sorted(bin_dict.keys())))

        # dump the dictionary with the letter digital image to yaml file
        with open(self.training_data_base, "w") as fp:
            yaml.dump(bin_dict, fp, default_flow_style=False)

    def load_training_letters(self):

        self.logger.info("Loading training data from file file {}".format(self.training_data_base))

        # dump the dictionary with the letter digital image to yaml file
        with open(self.training_data_base, "rb") as fp:
            bin_dict = yaml.load(fp, Loader=yaml.Loader)

        self.logger.debug("Pack dict keys {}".format(sorted(bin_dict.keys())))

        self.unpack_letters(bin_dict=bin_dict)


class ImageRecord(object):
    def __init__(self, pixels, key="ref", title="", colormap=cs.m_gray):
        self.logger = get_logger(__name__)
        self.pixels = pixels
        self.title = title
        self.key = key
        self.colormap = colormap

    def record_info(self):
        fmr = "{:20s} : {}"
        self.logger.info(fmr.format("title", self.title))
        self.logger.info(fmr.format("shaper", self.pixels.shape))
        self.logger.info(fmr.format("type", type(self.pixels.flatten()[0])))


class ImageDatabase(object):
    def __init__(self):
        self.logger = get_logger(__name__)

        self.image_list = OrderedDict()

    def add_image_to_dict(self, image, key, title="default", colormap=cs.m_gray):
        self.logger.debug("Adding to image data base: {}".format(key))
        self.image_list[key] = ImageRecord(pixels=image, key=key, colormap=colormap)

    def set_titles(self, title_list, char_is_blanco):
        ii = 0
        for cnt, (im_name, image) in enumerate(self.image_list.items()):
            if np.sum(image.pixels) // 255 > 10:
                if char_is_blanco[ii]:
                    blanco_char = "*"
                else:
                    blanco_char = ""
                image.title = chr(int(title_list[ii]) + ord("A")) + blanco_char
                ii += 1

    def print_info(self):
        for cnt, (im_name, image) in enumerate(self.image_list.items()):
            self.logger.info("++++++++ image {} {} +++++++++++++++".format(cnt, im_name))
            image.record_info()
            self.logger.info("\n")

    def plot_images(self, window_title="", n_rows=1, n_cols=None, im_width=4, im_height=4,
                    add_title=False,
                    transpose=False):

        if n_cols is None:
            n_cols = len(list(self.image_list.keys()))

        fig_width = n_cols * im_width
        fig_height = n_rows * im_height

        self.logger.debug("Init plot {} {} {} {}".format(n_cols, n_rows, fig_width, fig_height))

        fig, axis = plt.subplots(ncols=n_cols, nrows=n_rows, sharex=True, sharey=True,
                                 figsize=(fig_width, fig_height))

        fig.canvas.manager.set_window_title(window_title)

        i_row = 0
        j_col = 0
        for cnt, (im_name, image) in enumerate(self.image_list.items()):
            self.logger.debug("Plotting {:4d} {:4d} {:4d}".format(cnt, i_row, j_col))
            try:
                ax = axis[i_row][j_col]
            except (TypeError, IndexError):
                try:
                    ax = axis[j_col]
                except (TypeError, IndexError):
                    ax = axis

            ax.imshow(image.pixels, cmap=cs.m_gray)

            ax.axis("off")

            if add_title:
                ax.text(0.9, 0.9, "{}".format(image.title), transform=ax.transAxes,
                        fontdict=dict(color="red"))

            if not transpose:
                i_row += 1
                if i_row == n_rows:
                    i_row = 0
                    j_col += 1
            else:
                j_col += 1
                if j_col == n_cols:
                    j_col = 0
                    i_row += 1


def parse_command_line():
    """
    retrieve the options from the command line
    :return:  args, parser: the argument objecft and the parser it self
    """
    parser = argparse.ArgumentParser(description=
                                     "A script to count the letter in a word feut sheet",
                                     formatter_class=argparse.RawTextHelpFormatter)

    # list of file names. Minimum of one should be given
    parser.add_argument("file_name", help="Name of the image with the wordfeut screen shot",
                        action="store")
    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const",
                        dest="log_level",
                        const=logging.INFO)
    parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                        dest="log_level",
                        const=logging.WARNING)
    parser.add_argument("--save_chars", help="Save the characters to file", action="store_true")
    parser.add_argument("--show_debug_plot", help="Show the debug plot", action="store_true")
    parser.add_argument("--show_chars", help="Show the characters", action="store_true")
    parser.add_argument("--show_screenshots", help="Show the screensh", action="store_true")
    parser.add_argument("--use_sample_data", help="Use the sample data method", action="store_true")
    parser.add_argument("--simple_preprocess", help="Use simple algorithm for preprocessing",
                        action="store_true")
    parser.add_argument("--sample_data", help="Name of the sample data file",
                        default="letter-recognition.data")
    parser.add_argument("--tile_plot", help="Create the expesive tile plot", action="store_true")
    parser.add_argument("--data_base_name", help="Name of the training data base",
                        default="wordfeut_chars.yml")
    parser.add_argument("--mode", choices=["train", "count"],
                        help="Mode of script: 'train' or 'count'",
                        default="count")

    args = parser.parse_args()

    return args, parser


def tile_key(i_row, j_col):
    # create a key for a tile
    return "{},{}".format(i_row + 1, j_col + 1)


def get_bounding_box(img):
    """
    get the outer counter bounding box in the image
    :param img: a refefence to the img with a pattern in it
    :return: tuple with x_min, x_max, y_min, y_max
    """
    tmp = img.copy()
    try:
        contours, hierachy = cv2.findContours(tmp, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    except ValueError:
        im, contours, hierachy = cv2.findContours(tmp, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    x_min = x_max = y_min = y_max = None
    for cnt in contours:
        # M = cv2.moments(cnt)
        x_pos, y_pos, width, height = cv2.boundingRect(cnt)
        if x_min is None or x_pos < x_min:
            x_min = x_pos
        if x_max is None or x_pos + width > x_max:
            x_max = x_pos + width
        if y_min is None or y_pos < y_min:
            y_min = y_pos
        if y_max is None or y_pos + height > y_max:
            y_max = y_pos + height

    return x_min, x_max, y_min, y_max


def main():
    args, parser = parse_command_line()

    plt.ion()

    logger = create_logger(console_log_level=args.log_level)

    # create a letter data base.
    letter_db = LetterDatabase(file_name=args.file_name, training_data_base=args.data_base_name,
                               mode=args.mode, debug_plot=args.show_debug_plot)

    # show the screen shots
    if args.show_screenshots:
        letter_db.screen_shot_data_base.plot_images(im_width=4, im_height=8)

    if args.tile_plot:
        letter_db.tile_data_base.plot_images(n_cols=N_TILES, n_rows=N_TILES, im_width=1,
                                             im_height=1,
                                             add_title=True, transpose=True)

    if args.show_screenshots or args.tile_plot:
        plt.ioff()
        plt.show()


if __name__ == "__main__":
    main()
